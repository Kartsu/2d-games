using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Cinemachine;
using DG.Tweening;
using MLAPI;
using MLAPI.NetworkVariable;
using MLAPI.Messaging;

public class PlayerController : NetworkBehaviour
{

    Rigidbody2D _rb;

    bool _isJump = false;
    bool _isGrounded = false;
    bool _wasGrounded = false;

    float _stamina = 100.0f;
    [SerializeField]
    float _maxStamina = 100.0f;

    [SerializeField]
    float _revealDuration;

    [SerializeField]
    float _stealthDuration;

    [SerializeField]
    float _playerSpeed;
    [SerializeField]
    float _jumpForce;
    [SerializeField]
    float _dashSpeed;

    [SerializeField]
    float _dashCost = 33.0f;

    [SerializeField]
    float _dashTime;

    [SerializeField]
    float _jumpCooldownTime;

    [SerializeField]
    float _bottomOffset;

    bool _dashAxisInUse = false;

    bool _slashAxisInUse = false;

    bool _jumpCooldown;

    float _distanceToGround;

    Ability _ability;

    [SerializeField]
    float _dashCooldownTime;

    [SerializeField]
    bool _disableMovement;

    [SerializeField]
    LayerMask _layerMask;

    [SerializeField]
    float _gravity;

    Collider2D collider;

    CinemachineImpulseSource _shake;

    SpriteRenderer _sprite;

    [SerializeField]
    float _slashCooldownTime;

    bool _slashCooldown = false;

    bool _dashCooldown = false;

    Animator _anim;

    StaminaBar _staminaBar;

    [SerializeField]
    ParticleSystem _dashParticle;
    [SerializeField]
    ParticleSystem _slashParticle;

    [SerializeField]
    ParticleSystem _naturalParticle;

    [SerializeField]
    UnityEvent m_Dashed = default;
    public event UnityAction Dashed
    {
        add { m_Dashed.AddListener(value); }
        remove { m_Dashed.RemoveListener(value); }
    }

    [SerializeField]
    UnityEvent m_Landed = default;
    public event UnityAction Landed
    {
        add { m_Landed.AddListener(value); }
        remove { m_Landed.RemoveListener(value); }
    }
    void OnLanded()
    {
        _anim.SetBool("Grounded", true);
        m_Landed.Invoke();
    }
    void OnDashed()
    {
        /*
        Vector3 dashDirection = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        dashDirection = dashDirection - transform.position;
        Vector2 dashDirectionVector2 = new Vector2(dashDirection.x, dashDirection.y);
        float highestValue = Mathf.Max(Mathf.Abs(dashDirectionVector2.x), Mathf.Abs(dashDirectionVector2.y));
        //dashDirectionVector2.Normalize();
        ParticleSystem.MainModule newMain = _dashParticle.main;


        dashDirectionVector2.x = dashDirectionVector2.x / highestValue;
        dashDirectionVector2.y = dashDirectionVector2.y / highestValue;

        if (dashDirectionVector2.y > (1/3 * dashDirectionVector2.x)) {
            newMain.startRotationZ = (dashDirectionVector2.x + dashDirectionVector2.y + 1) * (Mathf.PI/4);
        }
        else
        {
            newMain.startRotationZ = Mathf.PI + (1-dashDirectionVector2.y - dashDirectionVector2.x) * (Mathf.PI / 4);
        }
        */
        //_dashParticle.GetComponent<ParticleSystemRenderer>().flip = new Vector3((dashDirectionVector2.x +1) /2, _dashParticle.GetComponent<ParticleSystemRenderer>().flip.y, _dashParticle.GetComponent<ParticleSystemRenderer>().flip.z);
        if (_stamina - _ability._staminaCost >= 0)
        {
            _stamina -= _dashCost;
            int dashDirection = 1;
            if (_sprite.flipX)
            {
                dashDirection = -1;
            }
            _dashParticle.GetComponent<ParticleSystemRenderer>().flip = new Vector3(dashDirection, 0, 0);
            m_Dashed.Invoke();
            _dashCooldown = true;
            _disableMovement = true;
            _rb.gravityScale = 0;
            //transform.DOMove(transform.position + dashDirection, _dashTime);
            _rb.velocity = new Vector2(dashDirection * _dashSpeed, _rb.velocity.y);
            _shake.GenerateImpulse();
            StartCoroutine(DashCooldown());
            StartCoroutine(MovementCooldown());
        }
    }

    [SerializeField]
    UnityEvent m_Melee = default;
    public event UnityAction Melee
    {
        add { m_Melee.AddListener(value); }
        remove { m_Melee.RemoveListener(value); }
    }
    void OnMelee()
    {
        if (_stamina - _ability._staminaCost >= 0)
        {
            _stamina -= _ability._staminaCost;
            _ability.UseAbility();
            m_Melee.Invoke();
            _slashCooldown = true;
            _anim.SetTrigger("Attack");
            int flip = 0;
            if (_sprite.flipX)
            {
                flip = 1;
            }
            ParticleSystemRenderer renderer = _slashParticle.GetComponent<ParticleSystemRenderer>();
            renderer.flip = new Vector3(flip, 0, 0);
            StartCoroutine(SlashCooldown());
        }
    }

    [SerializeField]
    UnityEvent m_Jumped = default;
    public event UnityAction Jumped
    {
        add { m_Jumped.AddListener(value); }
        remove { m_Jumped.RemoveListener(value); }
    }
    void OnJumped()
    {
        _jumpCooldown = true;
        _isJump = true;
        StartCoroutine(Cooldown());
        m_Jumped.Invoke();
        _shake.GenerateImpulse();
    }

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        collider = GetComponent<CapsuleCollider2D>();
        _distanceToGround = collider.bounds.extents.y;
        _shake = GetComponent<CinemachineImpulseSource>();
        _anim = GetComponent<Animator>();
        _sprite = GetComponent<SpriteRenderer>();
        _ability = GetComponent<Ability>();
        _stamina = _maxStamina;
        _staminaBar = GetComponentInChildren<StaminaBar>();
        _staminaBar.SetMaxStamina(_maxStamina);
        _staminaBar.SetStamina(_stamina);
        RevealThenStealth();
    }

    void OnMoved(float horizontalInput)
    {
        _anim.SetFloat("HorizontalMovement", Mathf.Abs(horizontalInput));
        _rb.velocity = new Vector2(_playerSpeed * horizontalInput, _rb.velocity.y);
        if (horizontalInput < 0)
        {
            _sprite.flipX = true;
        }
        else if (horizontalInput > 0)
        {
            _sprite.flipX = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        _isGrounded = IsGrounded();
        if (_isGrounded != _wasGrounded && _wasGrounded == false)
        {
            OnLanded();
        }
        if (!_isGrounded)
        {
            _anim.SetBool("Grounded", false);
        }

        if (IsOwner || IsLocalPlayer)
        {
            if (!_disableMovement)
            {
                float horizontalInput = Input.GetAxis("Horizontal");
                OnMoved(horizontalInput);
                SubmitMoveRequestServerRpc(horizontalInput, NetworkManager.Singleton.LocalClientId);
            }
            //_rb.AddForce(new Vector2(_playerSpeed * Input.GetAxis("Horizontal"), 0));
            if (Input.GetAxis("Fire1") > 0)
            {
                if (!_slashAxisInUse)
                {
                    if (!_slashCooldown)
                    {
                        _slashAxisInUse = true;
                        OnMelee();
                        SubmitMeleeRequestServerRpc(NetworkManager.Singleton.LocalClientId);
                    }
                }
            }

            else
            {
                _slashAxisInUse = false;
            }

            if (Input.GetAxis("Jump") > 0 && !_jumpCooldown)
            {
                if (_isGrounded)
                {
                    OnJumped();
                    SubmitJumpRequestServerRpc(NetworkManager.Singleton.LocalClientId);
                }
            }

            if (Input.GetAxis("Fire2") > 0)
            {
                if (!_dashAxisInUse)
                {
                    if (!_dashCooldown)
                    {
                        _dashAxisInUse = true;
                        if (_stamina - _dashCost >= 0)
                        {
                            OnDashed();
                            SubmitDashRequestServerRpc(NetworkManager.Singleton.LocalClientId);
                        }
                    }
                }

            }
            else
            {
                _dashAxisInUse = false;
            }

            //Debug.Log(NetworkManager.Singleton.ConnectedClients[NetworkManager.Singleton.LocalClientId].PlayerObject);
            //Position.Value = transform.position;
            //SubmitUpdatePositionRequestServerRpc(transform.position);
        }
        else
        {
            //transform.position = Position.Value;
        }
        _wasGrounded = _isGrounded;
        _staminaBar.SetStamina(_stamina);
    }

    bool IsGrounded()
    {
        return Physics2D.Raycast(transform.position, Vector3.down, _distanceToGround + _bottomOffset, _layerMask);
    }

    private void FixedUpdate()
    {
        _stamina = Mathf.Min(_maxStamina, _stamina + 0.1f);
        if (_isJump)
        {
            _rb.velocity = new Vector2(_rb.velocity.x, 0);
            _rb.AddForce(new Vector2(0, _jumpForce));
            _isJump = false;
        }

    }

    IEnumerator Cooldown()
    {
        yield return new WaitForSeconds(_jumpCooldownTime);
        _jumpCooldown = false;
    }

    IEnumerator SlashCooldown()
    {
        yield return new WaitForSeconds(_slashCooldownTime);
        _slashCooldown = false;
    }

    IEnumerator DashCooldown()
    {
        yield return new WaitForSeconds(_dashCooldownTime);
        _dashCooldown = false;
    }

    IEnumerator MovementCooldown()
    {
        yield return new WaitForSeconds(_dashTime);
        _disableMovement = false;
        _rb.gravityScale = _gravity;
        _rb.velocity = new Vector2(0, 0);
    }

    public void RevealThenStealth()
    {
        //_sprite.color.a
        float opacity = _sprite.color.a;
        Color col = _sprite.color;
        ParticleSystem.MainModule naturalMain = _naturalParticle.main;
        DOTween.To(() => opacity, x => opacity = x, 1, _revealDuration).OnUpdate(() =>
        {
            col.a = opacity;
            _sprite.color = col;
            Color tmp = naturalMain.startColor.color;
            tmp.a = opacity;
            naturalMain.startColor = tmp;
            _staminaBar.SetOpacity(opacity);
        }
        ).OnComplete(() =>
        {
            Stealth();
        }
        );
    }

    void Stealth()
    {
        float opacity = _sprite.color.a;
        Color col = _sprite.color;
        ParticleSystem.MainModule naturalMain = _naturalParticle.main;
        float stealthValue = 0;
        if (IsOwner || IsLocalPlayer)
        {
            stealthValue = 0.2f;
        }
        DOTween.To(() => opacity, x => opacity = x, stealthValue, _stealthDuration).OnUpdate(() =>
        {
            col.a = opacity;
            _sprite.color = col;
            Color tmp = naturalMain.startColor.color;
            tmp.a = opacity;
            naturalMain.startColor = tmp;
            _staminaBar.SetOpacity(opacity * 5.0f);
        }
        );
    }

    public NetworkVariableVector3 Position = new NetworkVariableVector3(new NetworkVariableSettings
    {
        WritePermission = NetworkVariablePermission.ServerOnly,
        ReadPermission = NetworkVariablePermission.Everyone
    });

    public override void NetworkStart()
    {
        Move();
        CinemachineTargetGroup targetGroup = FindObjectOfType<CinemachineTargetGroup>();
        targetGroup.AddMember(transform, 1.0f, 5.0f);
        StartCoroutine(Resynchronise());
    }

    public void Move()
    {
        if (NetworkManager.Singleton.IsServer)
        {
            var randomPosition = GetRandomPositionOnPlane();
            transform.position = randomPosition;
            Position.Value = randomPosition;
        }
        else
        {
            SubmitPositionRequestServerRpc();
        }
    }

    [ServerRpc]
    void SubmitPositionRequestServerRpc(ServerRpcParams rpcParams = default)
    {
        Position.Value = GetRandomPositionOnPlane();
    }

    [ServerRpc]
    void SubmitUpdatePositionRequestServerRpc(Vector3 position, ulong clientID)
    {
        //Position.Value = position;
        ReceiveUpdatePositionRequestClientRpc(position, clientID);
    }

    [ClientRpc]
    void ReceiveUpdatePositionRequestClientRpc(Vector3 position, ulong clientID)
    {
        if (clientID != NetworkManager.Singleton.LocalClientId)
        {
            transform.position = position;
        }
    }

    [ServerRpc]
    void SubmitMoveRequestServerRpc(float horizontalInput, ulong clientID)
    {
        ReceiveMoveRequestClientRpc(horizontalInput, clientID);
    }

    [ServerRpc]
    public void SubmitClientDisconnectServerRpc(ulong clientID)
    {
        ReceiveClientDisconnectClientRpc(clientID);
    }

    [ClientRpc]
    void ReceiveClientDisconnectClientRpc(ulong clientID)
    {
        if (clientID == NetworkManager.Singleton.LocalClientId)
        {
            NetworkManager.Singleton.StopClient();
        }
    }

    [ServerRpc]
    void SubmitMeleeRequestServerRpc(ulong clientID)
    {
        ReceiveMeleeRequestClientRpc(clientID);
    }

    [ClientRpc]
    void ReceiveMoveRequestClientRpc(float horizontalInput, ulong clientID)
    {
        if (clientID != NetworkManager.Singleton.LocalClientId)
        {
            OnMoved(horizontalInput);
        }
    }

    [ClientRpc]
    void ReceiveJumpRequestClientRpc(ulong clientID)
    {
        if (clientID != NetworkManager.Singleton.LocalClientId)
        {
            OnJumped();
        }
    }

    [ClientRpc]
    void ReceiveMeleeRequestClientRpc(ulong clientID)
    {
        if (clientID != NetworkManager.Singleton.LocalClientId)
        {
            OnMelee();
        }
    }

    [ClientRpc]
    void ReceiveDashRequestClientRpc(ulong clientID)
    {
        if (clientID != NetworkManager.Singleton.LocalClientId)
        {
            OnDashed();
        }
    }

    [ServerRpc]
    void SubmitJumpRequestServerRpc(ulong clientID)
    {
        ReceiveJumpRequestClientRpc(clientID);
    }

    [ServerRpc]
    void SubmitDashRequestServerRpc(ulong clientID)
    {
        ReceiveDashRequestClientRpc(clientID);
    }

    static Vector3 GetRandomPositionOnPlane()
    {
        return new Vector3(Random.Range(-3f, 3f), 0f, -10);
    }

    IEnumerator Resynchronise()
    {
        while (true)
        {
            //resync time is 2 seconds
            yield return new WaitForSeconds(2.0f);
            ReceiveUpdatePositionRequestClientRpc(transform.position, NetworkManager.Singleton.LocalClientId);
            //SubmitUpdatePositionRequestServerRpc(transform.position, NetworkManager.Singleton.LocalClientId);
        }
    }
}
