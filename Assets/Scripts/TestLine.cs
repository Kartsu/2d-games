using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TestLine : MonoBehaviour
{

    public float width = 0.05f;
    public Color color = Color.red;
    private LineRenderer lr;
    private Vector3[] linePoints = new Vector3[2];

    void Start()
    {
        lr = GetComponent<LineRenderer>();
        if (!lr) lr = gameObject.AddComponent<LineRenderer>();
        lr.material.color = color;
        lr.widthMultiplier = width;
        linePoints[0] = Vector3.zero;
        lr.positionCount = linePoints.Length;
    }

    void Update()
    {
        Camera c = Camera.main;
        Vector3 p = c.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10));
        linePoints[1] = p;
        lr.SetPositions(linePoints);

        if (Input.GetMouseButton(0))
        {
            //if (lr.enabled == false) lr.enabled = true;
            lr.enabled = false;
            linePoints[0] = p;
            transform.DOMove(p, 0.1f).SetEase(Ease.Linear).OnComplete(() =>
            {
                lr.enabled = true;
            });
        }
        else if (Input.GetMouseButtonUp(0))
        {
            //lr.enabled = false;
        }
    }

    void OnGUI()
    {
        GUI.skin.label.fontSize = 30;
        GUILayout.Label("Screen: " + Input.mousePosition.ToString());
        GUILayout.Label("World: " + linePoints[1].ToString());
    }

}